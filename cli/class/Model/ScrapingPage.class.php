<?php

class ScrapingPage extends ModelBase
{
    private $name = 'scrapingpage';
    private $data = array(
        'id' => '',
        'url' => '',
        'data' => '',
        'timestamp' => '',
    );
    
    public getId()
    {
        return $this->data['id'];
    }
    public getUrl()
    {
        return $this->data['url'];
    }
    public getData()
    {
        return $this->data['data'];
    }
    public getTimeStamp()
    {
        return $this->data['timestamp'];
    }

    public setId($id)
    {
        $this->data['id'] = $id;
    }
    public setUrl($url)
    {
        $this->data['url'] = $url;
    }
    public setData($data){
        $this->data['data'] = $data;
    }
    public setTimeStamp($timestamp)
    {
        $this->data['timestamp'] = $timestamp;
    }
}
