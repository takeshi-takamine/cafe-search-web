<?php

class ScrapingSite extends ModelBase
{
    private $name = 'scrapingsite';
    private $data = array(
        'id' => '',
        'url' => '',
        'category' => '',
    );
    
    public function getId()
    {
        return $this->data['id'];
    }
    public function getUrl()
    {
        return $this->data['url'];
    }
    public function getCategory()
    {
        return $this->data['category'];
    }
    public function setId($id)
    {
        $this->data['id'] = $id;
    }
    public function setUrl($url)
    {
        $this->data['url'] = $url;
    }
    public function setCategory($category){
        $this->data['category'] = $category;
    }   
}
