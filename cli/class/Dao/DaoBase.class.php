<?php

class DaoBase
{
    protected $db='cafe_search';
    protected $conn=null;

    private $conf = array(
        'user' => 'root',
        'pass' => 'eugene',
    );

    protected function connect()
    {
        if(is_null($this->conn[$this->db])) {
            $this->conn[$this->db] = new MySQL($this->db , $this->conf['user'], $this->conf['pass']);
            if(!$this->conn[$this->db]->executeSQL('SET NAMES utf8')) {
                throw new Exception('can not access db');
            }
        }
    }

    protected function disconnect()
    {
        if(!is_null($this->conn[$this->db])) {
            $this->conn[$this->db]->closeConnection();
        }
    }
}
