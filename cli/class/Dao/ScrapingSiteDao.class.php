<?php

class ScrapingSiteDao extends DaoBase
{

    function ScrapingSiteDao(){
        $this->connect();
    }

    function __destruct(){
        $this->disconnect();
    }

    public function getSiteList()
    {
        $result = $this->conn[$this->db]->select('scrapingsite', true, '','',false,'AND','*','string');
        return $result;
    }   
    public function getSiteById($id) {
        $site = array();
        $ret = $this->conn[$this->db]->select('scrapingsite', array('id'=>$id), '','',false,'AND','*',array('int'));
        if(!empty($ret)) {
            $site = new ScrapingSite();
            $site->setId($ret['id']);
            $site->setUrl($ret['url']);
            $site->setCategory($ret['category']);
        }
        return $site;    
    }
    public function insertData($param)
    {
        serialize ($param);
    }
 

}
