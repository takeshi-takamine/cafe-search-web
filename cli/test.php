<?php
include_once(dirname(__FILE__) . '/lib/class.MySQL.php');
include_once(dirname(__FILE__) . '/lib/simple_html_dom.php');
include_once(dirname(__FILE__) . '/lib/ScrapingBase.class.php');

include_once(dirname(__FILE__) . '/class/Dao/DaoBase.class.php');
include_once(dirname(__FILE__) . '/class/Dao/ScrapingSiteDao.class.php');
include_once(dirname(__FILE__) . '/class/Model/ModelBase.class.php');
include_once(dirname(__FILE__) . '/class/Model/ScrapingSite.class.php');

class main extends ScrapingBase
{
    CONST SITE_ID = 1;

    public function insert($url)
    {
        $param = array(
            'pref' => '東京都',
            'knktxtfidf' => 'bunen',
        );

        $context = $this->makePostContext($param);
        $this->loadContents($url, $context);
        $this->parser();
        $dom = $this->getdom();

        $data_keys = array(
            'line' => 'body table[class=table] tbody tr',
            'keys'  => array(
                'name'            => array('plaintext', 'td[class=storeName]'),
                'url'             => array('href', 'td[class=storeName] a'),
                'businessHours'   => array('plaintext', 'td[class=businessHours]'),
                'telephone'       => array('plaintext', 'td[class=telephone]'),
                'seats'           => array('plaintext', 'td[class=seats]'),
                'services'        => array('plaintext', 'td[class=services]'),
                'wirelessHotspot' => array('plaintext', 'td[class=wirelessHotspot]'),
            ), 
        );
        
        $data = array();
        $count =0;
        foreach($dom->find($data_keys['line']) as $element) {
            foreach($data_keys['keys'] as $key => $val) {
                $data[$count][$key] = $element->find($val[1], 0)->$val[0];
            }
            var_dump($data);
            if($count > 4) exit;
            $count++;
        }
        var_dump($data);
        print $count;
    }
}
$url= 'http://www.starbucks.co.jp/store/search/result_store.php?search_type=1&free_word=&pref_code=13&x=61&y=20';

$obj = new main();
$obj->insert($url);

