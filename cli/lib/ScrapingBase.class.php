<?php

class ScrapingBase {
    private $contents;
    private $dom;

    public function loadContents($url, $context=null) {
        if(!empty($context)){
            $this->contents = file_get_contents($url, false, $context);
        } else {
            $this->contents = file_get_contents($url);
        }
    }
    public function getDom() {
        return $this->dom;
    }
    public function parser() {
        $this->dom = str_get_html( $this->contents );
        //$this->dom = new simple_html_dom();
    }
    /*
     * param array
     * return resource
     * exsample: param 
     * array(
     *   'Item' => 'Value',
     *   'Item2' => 'Value2'
     * )
    */ 
    public function makePostContext($param){
        
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen(http_build_query($param)),
            'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
        );
        
        $request = array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $headers),
                'content' => http_build_query($param, "", "&"),
            ),
        );
        return stream_context_create($request);
    }
}
   
