<?php
ini_set( "display_errors", "Off");

require 'simple_html_dom.php';

$domain = "http://www.starbucks.co.jp";
$pref_code=13;    // 東京
$pageID_max=27;   //ページ数27

for($cnt=1; $cnt <= $pageID_max; $cnt++){
    $request_url = 'http://www.starbucks.co.jp/store/search/result_store.php?pref_code='. $pref_code .'&pageID=' . $cnt;
    
//var_dump($request_url);
    $html = file_get_html_ext($request_url);
    //var_dump($html);
    $dom = $html->find( 'tbody tr' );

    //var_dump($dom);
    $i=0;
    $array_link = array();
    foreach($dom as $e){
        // なぜか、trタグで3回まわるのよ
        if($e->find( 'td[class=storeName] a', 0)->href){
            $array_link[$i]['url'] = $e->find( 'td[class=storeName] a', 0)->href;
            $array_link[$i]['name'] =  $e->find( 'td[class=storeName] a', 0)->plaintext;
            $array_link[$i]['timeTable1'] =  $e->find( 'td[class=businessHours]', 0)->plaintext;
            $array_link[$i]['timeTable2'] =  $e->find( 'td[class=businessHours]', 1)->plaintext;
            $array_link[$i]['telephone'] =  $e->find( 'td[class=telephone]', 0)->plaintext;
            $array_link[$i]['seats'] =  $e->find( 'td[class=seats]', 0)->plaintext;
            $array_link[$i]['services'] =  $e->find( 'td[class=services]', 0)->plaintext;
            $array_link[$i]['wirelessHotspot'] =  $e->find( 'td[class=wirelessHotspot]', 0)->plaintext;

           foreach($e->find('td[class=services] img') as $element){
                $array_link[$i]['services_img'][] = $element->getAttribute('alt');
            }

            $i++;
        }
    }
    $html->clear();
    unset($dom);

    foreach($array_link as $key => $val){
        //
        usleep(300000);
        $url = $domain. $array_link[$key]['url'];
        $array_link[$key]['detail'] = _detail($url);
        $array_link[$key]['geo'] = getgeo($url);
    }

    echo json_encode($array_link)."\n";
    unset($array_link);
}

function _detail($url){
    $html = file_get_html_ext($url);
    $dom = $html->find( 'table[class=table mapTable] tbody' );

    $i=0;
    $array_link = array();
    foreach($dom as $e){
        foreach($e->find('td') as $e_d){
            $array_link[$i]['data'] = $e_d->plaintext;
            $j=0;
            foreach($e_d->find('span[class=lanService] img') as $element){
                $array_link[$i]['img'][] = $element->getAttribute('alt');
                $j++;
            }
            $i++;
        }
    }
    $html->clear();
    unset($dom);
    
    return $array_link;
}

function getgeo($url){
    
    $result = array("resLat" => 0, "Longitude" => 0);
    
    $handle = fopen($url, "r");
    while (($buffer = fgets($handle, 4096)) !== false) {
        if(preg_match("/var x=/", $buffer)){
            $buffer = preg_replace('/var x=/', "", $buffer);
            $buffer = preg_replace('/;/', "", $buffer);
            $buffer = preg_replace('/\\n/', "", $buffer);
            $x = $buffer;
        }
        if(preg_match("/var y=/", $buffer)){
            $buffer = preg_replace('/var y=/', "", $buffer);
            $buffer = preg_replace('/;/', "", $buffer);
            $buffer = preg_replace('/\\n/', "", $buffer);
            $y = $buffer;
        }
    
        if(!empty($x) && !empty($y)){
            break;
        }
    }
    
    // 緯度経度がGoogleの規格とあっているか
    $x_buff = round($x);
    $y_buff = round($y);
    if (strlen($x_buff) == 2 && strlen($y_buff) == 3) {
        $result = getWorldGeodetic($x, $y);
    } else {
        $result = getWorldGeodetic($x, $y);
    }
    fclose($handle);
    
    var_dump($result,$url);
    
    return $result;
}

function file_get_html_ext() {
    $dom = new simple_html_dom;
    $args = func_get_args();
    $context = stream_context_create(array('http' => array(
        'method' => 'GET',
        'header' => 'User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E)',//ユーザエージェント
    )));
    $dom->load(call_user_func_array('file_get_contents', array($args['0'], false, $context)), true);
    return $dom;
}

//----------------------------------------
/**
 *  DBデータ（日本測地） ⇒ Googleデータ（世界測地）
 */
function getWorldGeodetic($x, $y) {
    $y = $y / 3600;
    $x = $x / 3600;

    $resLat = ($y - $y * 0.00010695  + $x * 0.000017464 + 0.0046017);
    $resLng = ($x - $y * 0.000046038 - $x * 0.000083043 + 0.010040);

    return array("resLat" => $resLat, "Longitude" => $resLng);
}

