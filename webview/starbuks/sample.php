<?php
require '../simple_html_dom.php';

$domain = "http://www.starbucks.co.jp";

$request_url = 'http://www.starbucks.co.jp/store/search/detail.php?id=905';

$handle = fopen($request_url, "r");
while (($buffer = fgets($handle, 4096)) !== false) {
    if(preg_match("/var x=/", $buffer)){
        $buffer = preg_replace('/var x=/', "", $buffer);
        $buffer = preg_replace('/;/', "", $buffer);
        $buffer = preg_replace('/\\n/', "", $buffer);
        $x = $buffer;
    }
    if(preg_match("/var y=/", $buffer)){
        $buffer = preg_replace('/var y=/', "", $buffer);
        $buffer = preg_replace('/;/', "", $buffer);
        $buffer = preg_replace('/\\n/', "", $buffer);
        $y = $buffer;
    }

    if(!empty($x) && !empty($y)){
        break;
    }
}

// 緯度経度がGoogleの規格とあっているか
$x_buff = round($x);
$y_buff = round($y);
var_dump($x_buff, $y_buff);
if (strlen($x_buff) == 2 && strlen($y_buff) == 3) {
    $result = getWorldGeodetic($x, $y);
} else {
    $result = getWorldGeodetic($x, $y);
    /*
     var resY = (lat + lat*0.00010696 - lng*0.000017467 - 0.0046020) * 3600;
    var resX = (lng + lat*0.000046038 + lng*0.000083043 - 0.010040) * 3600;
    var min = getJapanGeodetic(latNE, lngNE);
    var max = getJapanGeodetic(latSW, lngSW);
    */
}

var_dump($result);

fclose($handle);
exit;

//----------------------------------------
/**
 *  DBデータ（日本測地） ⇒ Googleデータ（世界測地）
 */
function getWorldGeodetic($x, $y) {
    $y = $y / 3600;
    $x = $x / 3600;

    $resLat = ($y - $y * 0.00010695  + $x * 0.000017464 + 0.0046017);
    $resLng = ($x - $y * 0.000046038 - $x * 0.000083043 + 0.010040);

    return array("resLat" => $resLat, "Longitude" => $resLng);
}

    
