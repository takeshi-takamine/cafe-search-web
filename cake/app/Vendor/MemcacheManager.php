class MemcacheManager  {
        private static $cache = null;
        
        private function __construct(){}
        
        static function getInstance(){
                if(MemcacheManager::$cache == null){
                        MemcacheManager::$cache = new Memcache;
                        MemcacheManager::$cache -> connect('localhost', '11211');
                }
                return MemcacheManager::$cache;
        }
        function get($key){
                return MemcacheManager::$cache -> get($key);
        }
        function set($key, $var, $flag = null, $expire = '3600'){
                return MemcacheManager::$cache -> set($key, $var, $flag, $expire);
        }
        function close(){
                return MemcacheManager::$cache -> close();
        }       
	function flush() {
		return MemcacheManager::$cache -> flush();
	} 
}