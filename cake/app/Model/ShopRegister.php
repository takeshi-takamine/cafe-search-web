<?php
/*
 * 店舗情報登録総合データvalidate class
 */
class ShopRegister extends Model {

	public $validate = array(
	    'data' => array(
		    'rule'       => 'isJsonData',
			'required'   => true,
			'allowEmpty' => false,
			'message'    => '投稿データはjson形式で入力してください'
		),
    );
	
	public function isJsonData($data){
	    json_decode($data['data']);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
