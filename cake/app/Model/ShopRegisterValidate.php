<?php
/*
 * 店舗情報登録総合データvalidate class
 */
class ShopRegisterValidate extends Model {
	var $useTable = false;
		
	public $validate = array(
	    //shop_groups
			'greoup_id' => array(
			    'rule'       => array('numeric'),
				'required'   => false,
			    'allowEmpty' => true,
				'message'    => 'グループIDは、数値で入力してください'
			),
	    //shops
			'name' => array(
		        'rule'       => array('between',1,255),
				'required'   => true,
				'allowEmpty' => false,
				'message'    => 'お店の名前は、1文字以上125文字以内で入力してください'
			),
			'zip' => array(
		        'rule'       => array('custom', '/^[0-9]{3}[\s-]?[0-9]{4}$/'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '郵便番号は形式似合わせ入力してください(例：123-1234)'
	        ),
			'address' => array(
		        'rule'       => array('between',1,255),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '住所は、1文字以上125文字以内で入力してください'
			),
			'tel' => array(
		        'rule'       => array('custom', '/^(0\d{1,4}[\s-]?\d{1,4}[\s-]?\d{4})$/'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '電話番号を半角英数にて入力してください'
			),
			'url'            => array(
		        'rule'       => array('url'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => 'urlを入力してください'
			),
			'seats'          => array(
		        'rule'       => array('numeric'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '座席数を入力してください'
			),
			'wifi'           => array(
		        'rule'       => array('boolean'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => 'wifi利用は、ture/falseにて入力してください'
			),
			'timetable1'     => array(
		        'rule'       => array('between',1,255),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '営業時間帯は、1文字以上125文字以内で入力してください'
			),
			'timetable2'     => array(
		        'rule'       => array('between',1,255),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '営業時間帯は、1文字以上125文字以内で入力してください'
			),
			'address_detail' => array(
		        'rule'       => array('between',1,255),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '交通手段は、1文字以上125文字以内で入力してください'
			),
			'wifi_detail'    => array(
		        'rule'       => array('between',1,255),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => 'wifi環境については、1文字以上125文字以内で入力してください'
			),
			'services'       => array(
		        'rule'       => array('between',1,255),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => 'サービスについては、1文字以上125文字以内で入力してください'
			),
			'lat'            => array(
		        'rule'       => array('numeric'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '緯度経度を入力してください'
			),
			'lng'            => array(
		        'rule'       => array('numeric'),
				'required'   => false,
				'allowEmpty' => true,
				'message'    => '緯度経度を入力してください'
			),
    );
}
