<?php
class ManageshopseatsController extends AppController {
    public $components = array('RequestHandler');
    var $uses = array('Shop', 'Shop_detail', 'Shop_geometrie');

    public function index() {
    	if(!empty($this->request->query['shop_id'])){
    		$shop_id = $this->request->query['shop_id'];
    	} else {
    		return null;
    	}
        if(!empty($this->request->query['status'])){
    		$status = $this->request->query['status'];
    	} else {
    		return null;
    	}

        $query  = "INSERT INTO shop_seats (shop_id, status, count)"
                    . " VALUES (" . $shop_id . ", ". $status .",1)"
                    . " ON DUPLICATE KEY UPDATE count=count+1,ctime=NOW();";
        $result = $this->Shop->query($query, false);

        $result = array();
        $result['success'] = true;
        $result['data']['shop_id'] = $shop_id;

        $this->set(array(
                     'add' => $result,
                     '_serialize' => array('add')
        ));
    }
}
