<?php
/*
 * 店舗情報本登録 class
 */
class ManageshopsController extends AppController {
    public $components = array('RequestHandler');
    var $uses = array('Shop', 'Shop_detail', 'Shop_geometrie');

    public function index() {
        $this->set(array(
            'shop_details' => $shop_details,
            '_serialize' => array('shop_details')
        ));
    }
    
    public function view($id) {
        
        $result = array();
        if(empty($id)){
            $result['success'] = false;
            $result['data']['list'] = $array();
        } else {
        
            $query="SELECT * from shops where shop_id=".$id;

            $shops = $this->Shop->query($query);
            
            $query="SELECT * from shop_details where shop_id=".$id;
            $shop_details = $this->Shop_detail->query($query);
            
            $result['success'] = true;
            $result['data']['shop'] = $shops;
            $result['data']['shop_detail'] = $shop_details;
        }
        
        $this->set(array(
                     'shop' => $result,
                     '_serialize' => array('shop')
        ));
        
/*
        $shops = $this->Shop->findById($id);

        $this->set(array(
                'shop' => $shop,
                '_serialize' => array('shop')
        ));
*/
    }

    public function add() {

        $tables = array(
            'shops' => array(
                'name',
                'zip',
                'address',
                'tel',
                'url',
                'group_id',
                'seats',
                'wifi',
            ),
            'shop_details' => array(
                'timetable1',
                'timetable2',
                'address_detail',
                'wifi_detail',
                'services',
            ),
            'shop_geometries' => array(
                'latlng',
                'lat',
                'lng',
            ),
        );

        $array_data = array();
        foreach($tables as $key => $value) {
            foreach($value as $data_key) {
                if(isset($this->request->data[$data_key])) {
                    $array_data[$key][$data_key] = $this->request->data[$data_key];
                } else {
                    $array_data[$key][$data_key] = "";
                }
            }
        }

        // ���줤�ˤ��褦�Ȼפä��������
        foreach($array_data as $key => $value){
            switch($key){
                case 'shops':
                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_values[] = $data_value;
                        $data_str .= "?";
                    }

                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop->query($query, $data_values, false);
                    $query  = "select LAST_INSERT_ID() as shop_id";
                    $result = $this->Shop->query($query);

                    $shop_id = $result[0][0]['shop_id'];
                break;
                case 'shop_details':
                    $value['shop_id'] = $shop_id;

                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_values[] = $data_value;
                        $data_str .= "?";
                    }
                    
                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop_detail->query($query, $data_values, false);

                break;
                case 'shop_geometries':
                    $value['shop_id'] = $shop_id;
                    $value['latlng']  = " GeomFromText('POINT(". $value['lat'] ." ". $value['lng'] . ")')";
                                        
                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_str .= $data_value;
                    }

                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop_geometrie->query($query);
                    break;
            }
        }

        $result = array();
        $result['success'] = true;
        $result['data']['shop_id'] = $shop_id;

        $this->set(array(
                     'add' => $result,
                     '_serialize' => array('add')
        ));
    }

    public function edit($id) {
        $this->Recipe->id = $id;
        if ($this->Recipe->save($this->request->data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }

    public function delete($id) {
        if ($this->Recipe->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }
}
