<?php
class SearchshopsController extends AppController {
	public $components = array('RequestHandler');
    var $uses = array('Shop', 'Shop_detail', 'Shop_geometrie');

    public function index() {
        //100m 0.0009
        $m = 0.0009;
        $d = 1;
        $limit=1;
        $offset = 0;
        $lev=0;
        
        //http://dev-takamine.dev.gree-dev.net/cafesearch/cake/searchshops.json?lat=1&lng=3&d=100
        if(isset($this->request->query['lat'])) {
            $lat = $this->request->query['lat'];
        }
        if(isset($this->request->query['lng'])) {
            $lng = $this->request->query['lng'];
        }
        if(isset($this->request->query['d'])) {
            $d = $this->request->query['d'];
        }
        if(isset($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        }
        if(isset($this->request->query['offset'])) {
            $offset = $this->request->query['offset'];
        }
        if(isset($this->request->query['lev'])) {
            $lev = $this->request->query['lev'];
        }

        $key = serialize ($this->request->query);
/*        
        if($cache = Cache::read($key)){
            $this->set(array(
                         'shop_list' => $cache,
                         '_serialize' => array('shop_list')
            ));
            return;
        }
*/
        $m = $m * $d;

$query="SELECT SQL_CALC_FOUND_ROWS shop_id, x(latlng) as lat, y(latlng) as lng"
." FROM `shop_geometries`"
." WHERE MBRWithin(latlng, GeomFromText('LINESTRING(". ($lat+$m) ." ". ($lng-$m) .",". ($lat-$m) ." ". ($lng+$m) .")'))"
." AND distance_in_meter(latlng,"
." PointFromText('POINT (".$lat." ".$lng.")')) < ". ($d * 100)
." limit ". $limit ." offset ". $offset;

//var_dump($query);

/*
 * 
 ." distance_in_meter(latlng,"
." PointFromText('POINT (".$lat." ".$lng.")')) d"
 * 
 */

        $list = $this->Shop_geometrie->query($query);

        $arry_list = array();
        $arry_list_shop_id = array();
        foreach($list as $key => $value){
            $id = $value['shop_geometries']['shop_id'];

            $arry_list[$id]['shop_id'] = $id;
            $arry_list[$id]['lat'] = $value[0]['lat'];
            $arry_list[$id]['lng'] = $value[0]['lng'];

            if(!empty($lev)){
                $arry_list_shop_id[] = $value['shop_geometries']['shop_id'];
            }
        }

$query="SELECT FOUND_ROWS() as 'count'";
        
        $ret = $this->Shop_geometrie->query($query);
        $count = $ret[0][0]['count'];
        
        switch($lev){
            case 1:      // + shop_name
            	$arry_list = $this->setShopName($arry_list, $arry_list_shop_id);
                break;
            case 2:      // + shop_name + seat status
            	$arry_list = $this->setShopName($arry_list, $arry_list_shop_id);
                $arry_list = $this->setSeartStatus($arry_list, $arry_list_shop_id);
                break;
            case 3:      // + shop_name + seat status + image
                $arry_list = $this->setShopName($arry_list, $arry_list_shop_id);
                $arry_list = $this->setSeartStatus($arry_list, $arry_list_shop_id);
                $arry_list = $this->setShopImage($arry_list, $arry_list_shop_id, array(0));
                $arry_list = $this->setShopDtail($arry_list, $arry_list_shop_id, array('address_detail'));
                break;
                
        }

        var_dump($arry_list);
        
        $array_result =array();
        $index =0;
        foreach($arry_list as $key => $value){
            $array_result[$index] = $value;
            $index++;
        }

        $result = array();
        
        $result['success'] = true;
        $result['data']['list'] = $array_result;
        $result['data']['pagination'] = array(
            "page_count"    => floor($count / $limit),
            "current_page"  => ($offset / $limit)+1,
            "has_next_page" => $offset + $limit < $count,
            "has_prev_page" => $offset > $limit,
            "count"         => $count,
            "limit"         => $limit,
        );

        $key = serialize ($this->request->query);
        Cache::write($key, $result);


        $this->set(array(
             'shop_list' => $result,
             '_serialize' => array('shop_list')
        ));

//        debug($this->Geometrie->getDataSource()->getLog());
/*        
        $this->set('data', $data);
        
        $shop_details = $this->Shop_detail->find('all');
        $shops = $this->Shop->find('all');
        
        var_dump($shops);
        exit;
        $this->set(array(
                'shops' => $shops,
                '_serialize' => array('shops')
        ));
        
        $this->set(array(
                                'shop_details' => $shop_details,
                                '_serialize' => array('shop_details')
        ));
*/
    }
    
    public function view($id) {
        
        $result = array();
        if(empty($id)){
            $result['success'] = false;
            $result['data']['list'] = $array();
        } else {
        
            $query="SELECT * from shops where shop_id=".$id;

            $shops = $this->Shop->query($query);
            
            $query="SELECT * from shop_details where shop_id=".$id;
            $shop_details = $this->Shop_detail->query($query);
            
            $result['success'] = true;
            $result['data']['shop'] = $shops;
            $result['data']['shop_detail'] = $shop_details;
        }
        
        $this->set(array(
                     'shop' => $result,
                     '_serialize' => array('shop')
        ));
        
/*
        $shops = $this->Shop->findById($id);

        $this->set(array(
                'shop' => $shop,
                '_serialize' => array('shop')
        ));
*/
    }

    public function add() {

        $tables = array(
            'shops' => array(
                'name',
                'zip',
                'address',
                'tel',
                'url',
                'group_id',
                'seats',
                'wifi',
            ),
            'shop_details' => array(
                'timetable1',
                'timetable2',
                'address_detail',
                'wifi_detail',
                'services',
            ),
            'shop_geometries' => array(
                'latlng',
                'lat',
                'lng',
            ),
        );

        $array_data = array();
        foreach($tables as $key => $value) {
            foreach($value as $data_key) {
                if(isset($this->request->data[$data_key])) {
                    $array_data[$key][$data_key] = $this->request->data[$data_key];
                } else {
                    $array_data[$key][$data_key] = "";
                }
            }
        }

        // ���줤�ˤ��褦�Ȼפä��������
        foreach($array_data as $key => $value){
            switch($key){
                case 'shops':
                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_values[] = $data_value;
                        $data_str .= "?";
                    }

                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop->query($query, $data_values, false);
                    $query  = "select LAST_INSERT_ID() as shop_id";
                    $result = $this->Shop->query($query);

                    $shop_id = $result[0][0]['shop_id'];
                break;
                case 'shop_details':
                    $value['shop_id'] = $shop_id;

                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_values[] = $data_value;
                        $data_str .= "?";
                    }
                    
                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop_detail->query($query, $data_values, false);

                break;
                case 'shop_geometries':
                    $value['shop_id'] = $shop_id;
                    $value['latlng']  = " GeomFromText('POINT(". $value['lat'] ." ". $value['lng'] . ")')";
                                        
                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_str .= $data_value;
                    }

                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop_geometrie->query($query);
                    break;
            }
        }

        $result = array();
        $result['success'] = true;
        $result['data']['shop_id'] = $shop_id;

        $this->set(array(
                     'add' => $result,
                     '_serialize' => array('add')
        ));
    }

    public function edit($id) {
        $this->Recipe->id = $id;
        if ($this->Recipe->save($this->request->data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }

    public function delete($id) {
        if ($this->Recipe->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }
    
    /*
     * setShopName
     * @param array 
     * @param array
     * @return array
     */
    private function setShopName($arry_list, $arry_list_shop_id)
    { 
        $shop_ids = implode(",", $arry_list_shop_id);
        if(empty($shop_ids)){
        	return null;
        }
        
  	    $query="SELECT shop_id, name,url , group_id from shops where shop_id in (".$shop_ids.")";
   	    $shops = $this->Shop->query($query);
   	    foreach($shops as $key => $value){
    	   $id = $value['shops']['shop_id'];
    
    	   $arry_list[$id]['name']     = $value['shops']['name'];
    	   $arry_list[$id]['url']      = $value['shops']['url'];
    	   $arry_list[$id]['group_id'] = $value['shops']['group_id'];
    	}
    	return $arry_list;
    }   

    /*
     * setSeartStatus
     * @param array 
     * @param array
     * @return array
     */
    private function setSeartStatus($arry_list, $arry_list_shop_id)
    {
    	if(empty($arry_list_shop_id)){
    		return null;
    	}
    	if(!is_array($arry_list_shop_id)){
    		$arry_list_shop_id = array($arry_list_shop_id);
    	}

    	foreach($arry_list_shop_id as $shop_id) {
    		$expire_time = 60 * 5;
    		//TODO cache 入れるよ $shop_idの空席調査結果
    		$query="SELECT * from shop_seats where shop_id = ". $shop_id ." and ctime > (now() - " . $expire_time .") order by status";
    		$seat_status = $this->Shop->query($query);
    		
    		//$query="SELECT * from shop_seat_masters where shop_id in (".$shop_ids.")" and ;
    		//$seat_status_master = $this->Shop->query($query);

    		$status = 0;
    		foreach($seat_status as $value){
    			switch($value['shop_seats']['status']){
    				case -1: // 強制空席あり 
    					$status = -1;
    					break 2;  //foreach を抜ける
    				case 0:  // 通常営業
    					$status = 0;
    					break;
    				case 1:  // やや込み
    					// 時間内に1名以上の投稿があり反映
    					$status = 1;
    					break;
    				case 2:  // 混雑
    					// 時間内に3名以上の投稿があり反映
    					if($value['shop_seats']['count'] >= 3){
    						$status = 2;
    					}
    					break;
    			}
    		} 
    		$arry_list[$shop_id]['seat_status'] = $status;
    	}
    	return $arry_list;
    }

    /*
    * setShopImages
    * @param array
    * @param array
    * @return array
    */
    private function setShopImage($arry_list, $arry_list_shop_id, $no_array = null)
    {
    	if(empty($arry_list_shop_id)){
    		return null;
    	}
    	if(!is_array($arry_list_shop_id)){
    		$arry_list_shop_id = array($arry_list_shop_id);
    	}

    	// miss setting no_array
    	if(!empty($no_array)){
    		if(!is_array($no_array)){
    			return $arry_list;
    		}
    	}
    	
    	foreach($arry_list_shop_id as $shop_id) {
    		if(!empty($no_array)){
    			$no_string = implode(',' , $no_array);
    			$query="SELECT * from shop_images where shop_id = ". $shop_id 
    			        ." and `no` in ( ". $no_string . ")";
    			$images = $this->Shop->query($query);
    		} else {
    			$query="SELECT * from shop_images where shop_id = ". $shop_id;
    			$images = $this->Shop->query($query);
    		}
    		
    		$image_list = array();
    		foreach($images as $value) {
    			//TODO ファイルパス変更ここででしなくても良いけどね(仮置き 
    			// この変数は共通宣言しておくべき
    			$image_maps = array(
    			    'main',
    			    'detail1',
    				'detail2',
    				'detail3',
    			);
    			$path = 'http://www.starbucks.co.jp/coffee/images/';
    			$image_list[$image_maps[$value['shop_images']['no']]] = $path .$value['shop_images']['file_name'];
    		}
    		$arry_list[$shop_id]['show_image'] = $image_list;
    	}
    	return $arry_list;
    }

    /*
     * setShopDetails
    * @param array
    * @param array
    * @return array
    */
    private function setShopDtail($arry_list, $arry_list_shop_id, $set_keys = null)
    {
    	if(empty($arry_list_shop_id)){
    		return null;
    	}
    	if(!is_array($arry_list_shop_id)){
    		$arry_list_shop_id = array($arry_list_shop_id);
    	}
    	 
    	$array_detail = array();
    	foreach($arry_list_shop_id as $shop_id) {
    		$query="SELECT * from shop_details where shop_id=".$shop_id;
    		$shop_details = $this->Shop_detail->query($query);
    		
    		foreach($shop_details as $data){
    			foreach($data['shop_details'] as $key => $value){
    			    if(!empty($set_keys) && !in_array($key, $set_keys)){
    				    continue;
    			    }
    			    $array_detail[$key] = $value;
    			}
    		}
    		$arry_list[$shop_id]['details'] = $array_detail;
    	}
    	return $arry_list;
    }
    
}
