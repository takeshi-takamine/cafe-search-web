<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class SearchshopsController extends AppController {
    public $components = array('RequestHandler');
    var $uses = array('Shop', 'Shop_detail', 'Shop_geometrie');

    // 
    public function index() {
    }

    // 空席状況確認
    public function view($id) {
        
        $result = array();
        if(empty($id)){
            $result['success'] = false;
            $result['data']['list'] = $array();
        } else {
        
            $query="SELECT * from shops where shop_id=".$id;

            $shops = $this->Shop->query($query);
            
            $query="SELECT * from shop_details where shop_id=".$id;
            $shop_details = $this->Shop_detail->query($query);
            
            $result['success'] = true;
            $result['data']['shop'] = $shops;
            $result['data']['shop_detail'] = $shop_details;
        }
        
        $this->set(array(
                     'shop' => $result,
                     '_serialize' => array('shop')
        ));
        
/*
        $shops = $this->Shop->findById($id);

        $this->set(array(
                'shop' => $shop,
                '_serialize' => array('shop')
        ));
*/
    }

    public function add() {

        $tables = array(
            'shops' => array(
                'name',
                'zip',
                'address',
                'tel',
                'url',
                'group_id',
                'seats',
                'wifi',
            ),
            'shop_details' => array(
                'timetable1',
                'timetable2',
                'address_detail',
                'wifi_detail',
                'services',
            ),
            'shop_geometries' => array(
                'latlng',
                'lat',
                'lng',
            ),
        );

        $array_data = array();
        foreach($tables as $key => $value) {
            foreach($value as $data_key) {
                if(isset($this->request->data[$data_key])) {
                    $array_data[$key][$data_key] = $this->request->data[$data_key];
                } else {
                    $array_data[$key][$data_key] = "";
                }
            }
        }

        // きれいにしようと思ったが、後回し
        foreach($array_data as $key => $value){
            switch($key){
                case 'shops':
                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_values[] = $data_value;
                        $data_str .= "?";
                    }

                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop->query($query, $data_values, false);
                    $query  = "select LAST_INSERT_ID() as shop_id";
                    $result = $this->Shop->query($query);

                    $shop_id = $result[0][0]['shop_id'];
                break;
                case 'shop_details':
                    $value['shop_id'] = $shop_id;

                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_values[] = $data_value;
                        $data_str .= "?";
                    }
                    
                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop_detail->query($query, $data_values, false);

                break;
                case 'shop_geometries':
                    $value['shop_id'] = $shop_id;
                    $value['latlng']  = " GeomFromText('POINT(". $value['lat'] ." ". $value['lng'] . ")')";
                                        
                    $culumn   = "";
                    $data_str = "";
                    $data_values = array();
                    foreach($value as $data_key => $data_value){
                        if(!empty($culumn)){
                            $culumn   .= ",";
                            $data_str .= ",";
                        }
                        $culumn .= $data_key;
                        $data_str .= $data_value;
                    }

                    $query  = "INSERT INTO ". $key ."("
                    . $culumn . ") VALUES (" . $data_str .")";

                    $result = $this->Shop_geometrie->query($query);
                    break;
            }
        }

        $result = array();
        $result['success'] = true;
        $result['data']['shop_id'] = $shop_id;

        $this->set(array(
                     'add' => $result,
                     '_serialize' => array('add')
        ));
    }

    public function edit($id) {
        $this->Recipe->id = $id;
        if ($this->Recipe->save($this->request->data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }

    public function delete($id) {
        if ($this->Recipe->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }
}
