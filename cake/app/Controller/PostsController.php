<?php
class PostsController extends AppController {
	//public $scaffold;
    public $components = array('RequestHandler');
    public $helpers = array('html', 'Form');	
    
    public function index() {
        //$this->set('posts', $this->Post->find('all'));
    	
    	$params =array(
    		'order' => 'mtime desc',
            'limit' => 2,
    	);
    	
    	$this->set('posts', $this->Post->find('all',$params));
    	
        $this->set('title_for_layout', 'fight!');
        
    } 
}

