<?php
/*
 * 店舗情報仮登録 class
 */
class ShopRegistersController extends AppController {
    public $components = array('RequestHandler');
    var $uses = array('ShopRegister', 'ShopRegisterImages','ShopRegisterValidate');

    public function index() {
    }
    
    public function view($id) {
        
        $result = array();
        if(empty($id)){
            $result['success'] = false;
            $result['data']['list'] = $array();
        } else {
        
            $query="SELECT * from shops where shop_id=".$id;

            $shops = $this->Shop->query($query);
            
            $query="SELECT * from shop_details where shop_id=".$id;
            $shop_details = $this->Shop_detail->query($query);
            
            $result['success'] = true;
            $result['data']['shop'] = $shops;
            $result['data']['shop_detail'] = $shop_details;
        }
        
        $this->set(array(
                     'shop' => $result,
                     '_serialize' => array('shop')
        ));
        
/*
        $shops = $this->Shop->findById($id);

        $this->set(array(
                'shop' => $shop,
                '_serialize' => array('shop')
        ));
*/
    }

    public function add() {

        if(!$this->request->isPost()) {
        	/*
        	$this->set(array(
        			'shop' => $shop,
        			'_serialize' => array('shop')
        	));
        	*/
        	return ; 
        } 
        	
    	$this->ShopRegisterValidate->set($this->request->data);
    	if (!$this->ShopRegisterValidate->validates()) {
    		$this->set(array(
    				'message' => $message,
    				'_serialize' => array('message')
    		));
    		return;
    	}

    	// save other images
    	$param = $this->request->data;
    	$data = array();
    	foreach($this->request->data as $key => $val) {
    		if(!preg_match("/img/", $key)) {
    			$data[$key] = $val;
    		}
    	}
    	$param['data'] = json_encode($data);
    	$this->ShopRegister->set($param);
    	if ($this->ShopRegister->validates()) {

    		$this->ShopRegister->save($param);
    		$id = $this->ShopRegister->getLastInsertID();

    		// set images
    		$count = count($this->request->form['img']['size']);
    		for($i=0; $i < $count; $i++) {
    			if(!isset($this->request->form['img']['size'][$i])) continue;
    		
    			// アップロードされた画像か
    			if (is_uploaded_file($this->request->form['img']['tmp_name'][$i])){
    				$file_name = explode('.', $this->request->form['img']['name'][$i]);
    				// 保存
    				$param = array(
    					'id'              => $id,
    					'no'              => $i+1,
                        'filename'        => md5(microtime()) . '.' . $extension = end($file_name),
                        'contents'        => file_get_contents($this->request->form['img']['tmp_name'][$i]),
                        'filename_origin' => $this->request->form['img']['name'][$i],
                        'filetype'        => $this->request->form['img']['type'][$i],
                        'filesize'        => $this->request->form['img']['size'][$i],
    				);
    				
    				$this->ShopRegisterImages->set($param);
    				
    				$this->ShopRegisterImages->id = false;
    				$this->ShopRegisterImages->save();
    				$this->ShopRegisterImages->create();
    				
    			}
    		}
    	} else {
    		var_dump($this->ShopRegister->validationErrors);
    		// バリデーションが失敗した場合のロジックをここに書く
    	}
    }

    public function edit($id) {
        $this->Recipe->id = $id;
        if ($this->Recipe->save($this->request->data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }

    public function delete($id) {
        if ($this->Recipe->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
                'message' => $message,
                '_serialize' => array('message')
        ));
    }
}
