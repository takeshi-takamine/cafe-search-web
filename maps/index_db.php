<!DOCTYPE html>
<html>
  <head>
    <title>Localizing the Map</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <style>
      html, body, #maincontainer, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px;
        z-index : 1;
      }

    </style>
    <link rel="stylesheet" href="/cafesearch/maps/css/jquery.mobile-1.4.2.min.css"/>
    <script type="text/javascript" src="/cafesearch/maps/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/cafesearch/maps/js/jquery.googlemap.js"></script>
    <script type="text/javascript" src="/cafesearch/maps/js/jquery.mobile-1.4.2.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=ja"></script>

    <script>
    var map;
    var infowindow;
    var shop=[];
    var markersArray = [];
    var iterator = 0;

    var pyrmont = new google.maps.LatLng(35.660434441382016, 139.7293996810913);


var iconUrl = "/cafesearch/images/starbacks.gif";

var arriconUrl = [
"/cafesearch/images/s_p_empty_s.png",
"/cafesearch/images/s_p_full2_s.png",
"/cafesearch/images/s_p_caution_s.png",
"/cafesearch/images/s_p_full_s.png",
"/cafesearch/images/s_p_nomal_s.png",
];

/*
var iconOffset = new google.maps.Point(34, 34);
var iconPosition = new google.maps.Point(0, 0);
var iconSize = new google.maps.Size(72, 72);
var iconShadowSize = new google.maps.Size(37, 34);
var icon = new google.maps.MarkerImage(iconUrl, iconSize, iconPosition, iconOffset);
*/

    function callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        iterator=0;
        for (var i = 0; i < results.length; i++) {
            shop.push(results[i]);
            setTimeout('createMarker()', i * 80);
        }
      }
    }

    function createMarker() {
      var place = shop[iterator];
      var placeLoc = place.geometry.location;

      //select icon
      var randnum = Math.floor( Math.random() * 4 );

      var icon_url = arriconUrl[randnum];
      
      var marker = new google.maps.Marker({
        map: map,
        position: placeLoc,
        icon: icon_url,
        draggable:false,
        animation: google.maps.Animation.DROP,
      });

      //���Τ����������¸
      markersArray.push(marker);

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });

      google.maps.event.addListener(marker, 'mouseover', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);

          if (marker.getAnimation() != null) {
              marker.setAnimation(null);
          } else {
              marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        });
      
      google.maps.event.addListener(map, 'click', function(event) {
          placeMarker(event.latLng);
      });
      iterator++;
    }

    function setMarker(place) {
    	var placeNam = place.name;
        var placeLoc = place.geometry.location;
//console.log(placeNam, placeLoc);
        //select icon
        var randnum = Math.floor( Math.random() * 4 );
        var icon_url = arriconUrl[randnum];
        
        var marker = new google.maps.Marker({
          map: map,
          position: placeLoc,
          icon: icon_url,
          draggable:false,
          animation: google.maps.Animation.DROP,
        });

        //���Τ����������¸
        markersArray.push(marker);

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent("<center><br><a href=" + place.url +"target=\"blank\" data-transition=\"slide\">"+placeNam+"</a></center>");
          infowindow.open(map, this);
        });

    }
    
    function placeMarker(location) {
        var clickedLocation = new google.maps.LatLng(location);
        var marker = new google.maps.Marker({
            position: location,
            icon: goldStar, 
            map: map
        });
        var i;
        if(i > 4 || i == 0) i=0;

        var j = i + 1;
        marker.setTitle(j.toString());
        attachSecretMessage(marker, i);

        map.setCenter(location);
    }

 // The five markers show a secret message when clicked
 // but that message is not within the marker's instance data

 function attachSecretMessage(marker, number) {
   var message = ["This","is","the","secret","message"];
   var infowindow = new google.maps.InfoWindow(
       { content: message[number],
         size: new google.maps.Size(50,50)
       });
   google.maps.event.addListener(marker, 'click', function() {
     infowindow.open(map,marker);
   });
 }

 function clearMap() {
    for (var i = 0; i < markersArray.length; i++ ) 
    {
        markersArray[i].setMap(null);
    }

    shop = [];
    markersArray = [];

 }

 function myclick(num) 
 {
     $.mobile.changePage('#home');
     google.maps.event.trigger(markersArray[num], "click");
     map.panTo(shop[num].geometry.location);
     infowindow.setContent(shop[num].name);
     infowindow.open(map);
 }

 function setNowPosition() 
 {
    clearMap();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showNowPosition, errNowPosition);
    }
 }

 function errNowPosition(error) 
 {
    var err_msg = "";
    switch(error.code)
    {
    case 1:
      err_msg = "位置情報の利用が許可されていません";
      break;
    case 2:
      err_msg = "デバイスの位置が判定できません";
      break;
    case 3:
      err_msg = "タイムアウトしました";
      break;
    }

    console.log(error.message);
    alert('Not Get Location');
 }

 // 現在の位置に移動
 function showNowPosition(position) 
 {
     var lat = position.coords.latitude;
     var lng = position.coords.longitude;

     var location =  new google.maps.LatLng(lat, lng);
     map.panTo(location);
     resetMap();
 }

 function resetMap() 
 {
    clearMap();
    var mapcenter = map.getCenter();
    mapcenterlng = mapcenter.lng(); //経度
    mapcenterlat = mapcenter.lat(); //緯度
    
    $url ="/cafesearch/cake/searchshops.json";
    $.getJSON($url, { lat: mapcenterlat, lng: mapcenterlng, d:5, limit:10, lev:1 }, function(json){
        if(json.shop_list.success){
            var list = json.shop_list.data.list;
            for ( var i = 0; i < list.length; ++i ) {

                var local_place= { 
                        'name':list[i].name,
                        'url' :list[i].url,
                        'geometry' :
                            {'location': new google.maps.LatLng(list[i].lat, list[i].lng)}
                 };

                setMarker(local_place);
                shop.push(local_place);
            }
        }
        //alert("JSON Data: " + json.shop_list.success);
    });
    return false;
 }
 
function toggleListPage() {
    var sidebarhtml="";
   
    $("#shopList").html('');
    for (var i = 0; i < markersArray.length; i++ )
    {
        var name = shop[i].name;

sidebarhtml += '<div data-role="collapsible"><h3>'+ name +'</h3><p><a href="javascript:myclick(' + i + ')">地図</a></p></div>';
    }
    $("#shopList").html(sidebarhtml);
    $.mobile.changePage('#page2');
    return false;
}

$(function(){   
   map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
  infowindow = new google.maps.InfoWindow();

  setNowPosition();
});

//http://tilotiti.github.io/jQuery-Google-Map/
      </script>
      </head>
      <body>
      <div id="home" data-role="page">

        <div data-role="header">
            <h1>Tiara</h1>
            <a href="/cafesearch/maps/index_db.php" class="ui-btn-right ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-right ui-icon-gear">Options</a>
        </div><!-- /header -->

        <div id="maincontainer" >
            <div id="map-canvas" style="width: 380px; height: 380px;"></div>
        </div>

        <div data-role="footer">
            <div data-role="navbar"  data-iconpos="bottom">
            <ul>
                <li><a href="#" data-icon="grid" onclick="setNowPosition();">現在位置</a></li>
                <li><a href="#" data-icon="grid" onclick="resetMap();">表示位置</a></li>
                <li><a href="#" data-icon="star" onclick="toggleListPage();">LIST表示</a></li>
            </ul>
            </div><!-- /navbar -->
        </div><!-- /footer -->

      <a href="#page2" data-transition="slide">Goto Page2</a>
      <a href="#page3">Goto page3</a>

      <a href="index_post.html">Goto manage</a>

      </div><!-- /page -->

      <div id="page2" data-role="page2">
          <div id="shopList" data-role="collapsible-set"></div>
          <a href="#home">Goto Home</a>
          <a href="#page3">Goto page3</a>
      </div>

      <div id="page3" data-role="page3">
         <a href="#home">Goto Home</a>
         <a href="#page2">Goto page2</a>
      </div>


      </body>
    </html>
