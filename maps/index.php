<!DOCTYPE html>
<html>
  <head>
    <title>Localizing the Map</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body, #maincontainer, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px;
        z-index : 1;
      }

#side_bar {
  position:absolute;
  right:0px;
  top : 0px;
  background-color:white;
  overflow-y: auto;
  overflow-x: hidden;
  z-index : 2;
}

    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&libraries=places&language=ja"></script>
    <script>
    var map;
    var infowindow;
    var shop=[];
    var markersArray = [];
    var iterator = 0;

    var pyrmont = new google.maps.LatLng(35.660434441382016, 139.7293996810913);
    //var pyrmont = new google.maps.LatLng(35.74128460000001, 139.7770998);

    var goldStar_open = {
        path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
        fillColor: "yellow",
        fillOpacity: 0.8,
        scale: 1,
        strokeColor: "gold",
        strokeWeight: 14
    };
    var goldStar_middle = {
            path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
            fillColor: "blue",
            fillOpacity: 0.8,
            scale: 1,
            strokeColor: "gold",
            strokeWeight: 14
        };
    var goldStar_close = {
            path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
            fillColor: "red",
            fillOpacity: 0.8,
            scale: 1,
            strokeColor: "gold",
            strokeWeight: 14
        };

var iconUrl = "http://dev-takamine.dev.gree-dev.net/~takeshi-takamine/images/starbacks.gif";

var arriconUrl = [
"http://dev-takamine.dev.gree-dev.net/~takeshi-takamine/images/s_p_empty.png",
"http://dev-takamine.dev.gree-dev.net/~takeshi-takamine/images/s_p_full2.png",
"http://dev-takamine.dev.gree-dev.net/~takeshi-takamine/images/s_p_caution.png",
"http://dev-takamine.dev.gree-dev.net/~takeshi-takamine/images/s_p_full.png",
"http://dev-takamine.dev.gree-dev.net/~takeshi-takamine/images/s_p_nomal.png",
];

var iconOffset = new google.maps.Point(34, 34);
var iconPosition = new google.maps.Point(0, 0);
var iconSize = new google.maps.Size(72, 72);
var iconShadowSize = new google.maps.Size(37, 34);
var icon = new google.maps.MarkerImage(iconUrl, iconSize, iconPosition, iconOffset);

    function initialize() {
      document.getElementById("side_bar").style.display = "none";

      map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var request = {
        location: pyrmont,
        radius: 300,       //����Ⱦ���ϰ�(�᡼�ȥ�)
        types: ['cafe']
      };
      infowindow = new google.maps.InfoWindow();
      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, callback);
    }

    function callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        iterator=0;
        for (var i = 0; i < results.length; i++) {
            shop.push(results[i]);
            setTimeout('createMarker()', i * 80);
        }
      }
    }

    function createMarker() {
      var place = shop[iterator];
      var placeLoc = place.geometry.location;

      //select icon
      var randnum = Math.floor( Math.random() * 4 );

       var icon_url = arriconUrl[randnum];
      
      var marker = new google.maps.Marker({
        map: map,
        position: placeLoc,
        icon: icon_url,
        draggable:false,
        animation: google.maps.Animation.DROP,
      });

      //���Τ����������¸
      markersArray.push(marker);

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });

      google.maps.event.addListener(marker, 'mouseover', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);

          if (marker.getAnimation() != null) {
              marker.setAnimation(null);
          } else {
              marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        });
      
      google.maps.event.addListener(map, 'click', function(event) {
          placeMarker(event.latLng);
      });
      iterator++;
    }

    function placeMarker(location) {
        var clickedLocation = new google.maps.LatLng(location);
        var marker = new google.maps.Marker({
            position: location,
            icon: goldStar, 
            map: map
        });
        var i;
        if(i > 4 || i == 0) i=0;

        var j = i + 1;
        marker.setTitle(j.toString());
        attachSecretMessage(marker, i);

        map.setCenter(location);
    }

 // The five markers show a secret message when clicked
 // but that message is not within the marker's instance data

 function attachSecretMessage(marker, number) {
   var message = ["This","is","the","secret","message"];
   var infowindow = new google.maps.InfoWindow(
       { content: message[number],
         size: new google.maps.Size(50,50)
       });
   google.maps.event.addListener(marker, 'click', function() {
     infowindow.open(map,marker);
   });
 }

 function clearMap() {
    for (var i = 0; i < markersArray.length; i++ ) 
    {
        markersArray[i].setMap(null);
    }

    shop = [];
    markersArray = [];

 }

 function resetMap() {
    var get_location = map.getCenter();

    clearMap();
    var request = {
        location: get_location,
        radius: 300,
        types: ['cafe']
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, callback);
 }

 function myclick(num) 
 {
     google.maps.event.trigger(markersArray[num], "click");
     $("#side_bar").hide(300);
     map.panTo(shop[num].geometry.location);
     infowindow.setContent(shop[num].name);
     infowindow.open(map);

 }

 function setNowPosition() 
 {
    navigator.geolocation.getCurrentPosition(showNowPosition, errNowPosition);
 }

 function errNowPosition(error) 
 {
    console.log(error.message);
    alert('Not Get Location');
 }

 // ���֤����Ф��줿����١����١��?�Ȼ��֤�ɽ��
 function showNowPosition(position) 
 {
     var lat = position.coords.latitude;
     var lng = position.coords.longitude;

     var location =  new google.maps.LatLng(lat, lng);
     map.panTo(location);

     resetMap()
 }

 //
 function hiddenList() 
 {
     $("#side_bar").hide(500);
     //document.getElementById("side_bar").style.display = "none";
 }

 function showList() {

    var sidebarhtml="";
    for (var i = 0; i < markersArray.length; i++ ) 
    {
	var name = shop[i].name;

sidebarhtml += '<table border="1" cellspacing="2" cellpadding="2" id="HPB_TABLE_2_A_101114020233" class="hpb-cnt-tb1"> \
<thead> \
<tr> \
<td class="hpb-cnt-tb-cell2" width="220"><a href="javascript:myclick(' + i + ')">' + name + '</a></td> \
</tr> \
</thead> \
<tbody>\
</table>';

    }

    sidebarhtml += '<input type="button" value="hidden list" onclick="hiddenList();return false;">';

    $("#side_bar").show(500);
    //document.getElementById("side_bar").style.display = "";
    document.getElementById("side_bar").innerHTML = sidebarhtml;
 }

   google.maps.event.addDomListener(window, 'load', initialize);

        </script>
      </head>
      <body>
      <div id="maincontainer">
        <div id="map-canvas" style="height:90%;"></div>
        <div style="width: 60%;  margin: 0 auto;">
            <input type="button" value="set Now" onclick="setNowPosition();return false;">
            <input type="button" value="reSet Point" onclick="resetMap();return false;">
            <input type="button" value="Point Clear" onclick="clearMap();return false;">
            <input type="button" value="show list" onclick="showList();return false;">
            <input type="button" value="hidden list" onclick="hiddenList();return false;">
        </div>
        <div id="side_bar" style="font-size : 12px;width : 250px;height : 635px;overflow : scroll;"></div>
      </div>
      </body>
    </html>

